"use strict";

var Users = function() {
  let users = [{
      first_name: "Josephine",
      last_name: "Robinson",
      birthday: "1996-09-26",
    },
    {
      first_name: "Dean",
      last_name: "long",
      birthday: "1984-10-23",
    },
    {
      first_name: "Sonia",
      last_name: "Holmes",
      birthday: "1958-06-21",
    },
    {
      first_name: "June",
      last_name: "Mcdonalid",
      birthday: "1960-05-06",
    },
    {
      first_name: "ella",
      last_name: "Lane",
      birthday: "1991-12-11",
    },
    {
      first_name: "Felecia",
      last_name: "Stone",
      birthday: "1958-04-21",
    },
    {
      first_name: "Elmer",
      last_name: "George",
      birthday: "1987-12-10",
    }
  ];

  this.addNew = function(user) {
    user.first_name = toCapitalCase(user.first_name);
    user.last_name = toCapitalCase(user.last_name);
    return users.push(user);
  }

  function validation(paramName, validLength) {
    let paramValue;
    do {
      if (paramName === "birthday") {
        paramValue = prompt(`Enter user's ${paramName} in the format 2019-11-21`);
      } else {
        paramValue = prompt(`Enter user's ${paramName} (minimum ${validLength} characters)`);
      }
    } while (typeof paramValue === "string" && paramValue.trim().length < validLength);
    return paramValue;
  }

  this.addUsers = function() {
    let usersNumber = parseInt(prompt('Enter number of users'));

    let userData = [{
        paramName: "first_name",
        validLength: 3
      },
      {
        paramName: "last_name",
        validLength: 3
      },
      {
        paramName: "birthday",
        validLength: 10
      }
    ];
    let i = 0;
    let parameter;
    for (i; i < usersNumber; i++) {
      let user = {};
      for (parameter in userData) {
        let result = validation(userData[parameter].paramName, userData[parameter].validLength);
        if (!result) {
          return alert("Operation was canceled")
        }
        user[userData[parameter].paramName] = result;
      }
      this.addNew(user);
    }
  }

  function toCapitalCase(string) {
    return string.charAt(0).toUpperCase() + string.substring(1);
  }

  function normalizeUsers() {
    users.forEach(function(user) {
      user.first_name = toCapitalCase(user.first_name);
      user.last_name = toCapitalCase(user.last_name);
    })
  }

  this.filterByFirstLetter = function(letter) {
    return users.filter(function(user) {
      return user.first_name.charAt(0) === letter || user.last_name.charAt(0) === letter
    });
  }

  this.sortByAge = function(items) {
    let array = items ? items : users
    return array.sort(function(a, b) {
      return new Date(a.birthday) - new Date(b.birthday);
    });
  }

  this.filterByAge = function(yearLimit) {
    let filtereUsers = users.filter(function(user) {
      let age = Math.floor((Date.now() - new Date(user.birthday)) / 31536000000)
      if (age > yearLimit) {
        return true
      }
    });
    return this.sortByAge(filtereUsers)
  }

  normalizeUsers()
}

var users = new Users();
users.addUsers();
console.log(users.filterByFirstLetter("E"));
console.log(users.filterByAge(30))